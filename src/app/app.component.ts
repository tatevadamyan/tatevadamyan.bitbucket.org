import { Component } from '@angular/core';

export class Number{
	id: number;
	name: string;
	description: string;
}

const Numbers: Number[] = [
	{id: 1, name: "Components", description: `
				An Angular class responsible for exposing data to a view and handling 
				most of the view’s display and user-interaction logic.`
	},
	{id: 2, name: "Services", description:`
				A service is used when a common functionality needs to be 
				provided to various modules.
	`},
	{id: 3, name: "Routing", description:`
				Routing helps in directing users to different pages based on the 
				option they choose on the main page. Hence, based on the option 
				they choose, the required Angular Component will be rendered to the user.
	`},
	{id: 4, name: "Data binding", description:`
				Applications display data values to a user and respond to user 
				actions (such as clicks, touches, and keystrokes). In data binding,
				you declare the relationship between an HTML widget and data source
				and let the framework handle the details. Data binding is an alternative
				to manually pushing application data values into HTML, attaching event listeners,
				pulling changed values from the screen, and updating application data values.
	`},
	{id: 5, name: "Observables", description:`
				An array whose items arrive asynchronously over time. Observables help 
				you manage asynchronous data, such as data coming from a backend service. 
				Observables are used within Angular itself, including Angular's event system 
				and its HTTP client service.
	`},
	{id: 6, name: "Forms module", description:"The ng module for forms."},

	{id: 7, name: "Directives ", description:`
				An Angular class responsible for creating, reshaping, and interacting with 
				HTML elements in the browser DOM. The directive is Angular's most fundamental 
				feature. A directive is usually associated with an HTML element or attribute. 
				This element or attribute is often referred to as the directive itself.
	`}
];

@Component({
  selector: 'app-root',
  template: `
	  <h1>{{title}}</h1>
	  <ul class="numbers">
	  	<li *ngFor="let num of numbers"
	  		[class.selected]="number === selectedNumb"
	  		(click) = "onSelect(num)">
			<span class="badge">{{num.id}}</span>{{num.name}}
			
	  	</li>
	  </ul>
		<div *ngIf="selectedNumber" class="descrip">	
			<h2>{{selectedNumber.name}}</h2>
			<p>{{selectedNumber.description}}</p>
		</div>
	`,
	styles:[`
		h1{
			text-align: center;
		}
		.numbers {
			margin: 0 0 2em 0;
	        list-style-type: none;
	        padding: 0;
	        width: 15em;
	        float:left;
		}
		.numbers li {
	      cursor: pointer;
	      background-color: #EEE;
	      margin: .5em;
	      padding: .3em 0;
	      height:1.6em;
	      border-radius: 4px;
	    }
		.numbers li.selected:hover {
	      background-color: #65959b ;
	      color: white;
	    }
	    .numbers .badge {
	      display: inline-block;
	      font-size: small;
	      color: white;
	      padding: 0.8em 0.7em 0 0.7em;
	      background-color: #607D8B;
	      line-height: 1em;
	      position: relative;
	      left: -1px;
	      top: -4px;
	      height: 1.8em;
	      margin-right: .8em;
	      border-radius: 4px 0 0 4px;
	    }
	    .descrip {
	    	line-height: 1em;
	    	padding:0.1em;
	    	// text-align: center;
	    	// font-family:san-serif;
	    }
	`]
})

export class AppComponent {
  title = 'Homework';
  numbers = Numbers;
  selectedNumber: Number;

  onSelect(number: Number):void{
	this.selectedNumber = number;
  }

}

